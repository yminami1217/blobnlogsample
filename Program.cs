using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using NLog;
using NLog.Web;


namespace BlobNLog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults((webBuilder) =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging =>
                    logging.AddAzureWebAppDiagnostics())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    UpdateNLogConfig(config.Build());
                })
                .UseNLog()
                ;
        
        private static void UpdateNLogConfig(IConfiguration configuration) {
            var storageConnectionString = configuration.GetSection("Storage:ConnectionString").Get < string > ();
            GlobalDiagnosticsContext.Set("StorageConnectionString", storageConnectionString);
            var configFile = "nlog.config";
            LogManager.Configuration = LogManager.LoadConfiguration(configFile).Configuration;
        }

    }
}
